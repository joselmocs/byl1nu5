﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace ilikecheats
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.timerWarz.Enabled = true;
            string[] data = DateTime.Today.AddDays(-1).ToShortDateString().Split('/');
            this.labelDate.Text = string.Format("{0}-{1}-{2}", data[1], data[0], data[2]);
        }

        private void timerWarz_Tick(object sender, EventArgs e)
        {
            Process[] p = Process.GetProcessesByName("WarZ");
            if (p.Length > 0)
            {
                this.labelStartGame.Text = "Injecting...";
                this.timerClose.Enabled = true;
            }
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void timerClose_Tick(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
