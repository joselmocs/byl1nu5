﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace ilikecheats
{
    public partial class Form1 : Form
    {
        private delegate void InvokeDelegate();

        public Form1()
        {
            InitializeComponent();
        }

        public void UpdateProgressBar()
        {
            Thread.Sleep(100);

            if (InvokeRequired)
            {
                Invoke(new InvokeDelegate(UpdateProgressBar));
                return;
            }

            progressBar1.Value = 3;
            Thread.Sleep(1000);

            progressBar1.Value = 20;
            Thread.Sleep(1000);

            progressBar1.Value = 40;
            Thread.Sleep(1000);

            progressBar1.Value = 70;
            Thread.Sleep(1000);

            progressBar1.Value = 90;
            Thread.Sleep(1000);

            progressBar1.Value = 100;
            Thread.Sleep(1000);

            label1.Text = "Updated!";

            SecondWindow();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread update = new Thread(new ThreadStart(Updater.Start));
            update.Start();

            Thread progressBar = new Thread(new ThreadStart(UpdateProgressBar));
            progressBar.Start();
        }

        private void SecondWindow()
        {
            Thread.Sleep(1000);
            Form2 form2 = new Form2();
            form2.Show();
            Thread.Sleep(300);
            this.Hide();
        }
    }
}
