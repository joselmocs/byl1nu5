﻿using System;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;

namespace ilikecheats
{
    public class Security
    {
        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern bool IsDebuggerPresent();

        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern bool CheckRemoteDebuggerPresent(Process process, bool present);

        public Security()
        {
            bool IsDbgPresent = false;
            IsDbgPresent = CheckRemoteDebuggerPresent(Process.GetCurrentProcess(), IsDbgPresent);
            if (IsDebuggerPresent() || IsDbgPresent || Debugger.IsAttached)
            {
                Environment.Exit(1);
            }

            new InternetConnection();
        }
    }

    public class InternetConnection
    {
        public bool available;

        public InternetConnection()
        {
            bool available = false;

            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface nic in nics)
            {
                if ((nic.NetworkInterfaceType != NetworkInterfaceType.Loopback && nic.NetworkInterfaceType != NetworkInterfaceType.Tunnel) &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    available = true;
                }
            }

            if (!available)
            {
                MessageBox.Show("Error: There was no internet connection.");
                Environment.Exit(0);
            }
        }
    }

    public class SingleGlobalInstance : IDisposable
    {
        public bool hasHandle = false;
        Mutex mutex;

        private void InitMutex()
        {
            string appGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value.ToString();
            string mutexId = string.Format("Global\\{{{0}}}", appGuid);
            mutex = new Mutex(false, mutexId);

            var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
            var securitySettings = new MutexSecurity();
            securitySettings.AddAccessRule(allowEveryoneRule);
            mutex.SetAccessControl(securitySettings);
        }

        public SingleGlobalInstance(int TimeOut)
        {
            InitMutex();
            try
            {
                if (TimeOut <= 0)
                    hasHandle = mutex.WaitOne(Timeout.Infinite, false);
                else
                    hasHandle = mutex.WaitOne(TimeOut, false);

                if (hasHandle == false)
                    Environment.Exit(1);
            }
            catch (AbandonedMutexException)
            {
                hasHandle = true;
            }
        }

        public void Dispose()
        {
            if (mutex != null)
            {
                if (hasHandle)
                    mutex.ReleaseMutex();
                mutex.Close();
            }
        }
    }

}
