﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ilikecheats
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (new SingleGlobalInstance(1000))
            {
                new Security();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
        }
    }
}
