﻿using System;
using System.Windows.Forms;

namespace byl1nu5
{
    public static class Program
    {
        public static string[] args;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Program.args = args;
            using (new SingleGlobalInstance(1000))
            {
                Core.Instance().Initialize();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new AForm());
            }
        }
    }
}
