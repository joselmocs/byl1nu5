﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;


namespace byl1nu5
{
    public partial class AForm : Form
    {
        public AForm()
        {
            InitializeComponent();
            Core.Instance().form = this;
            Core.Instance().Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.toolStripStatusLocalVersion.Text = Assembly.GetEntryAssembly().GetName().Version.ToString();
            this.toolStripStatusRemoteVersion.Text = Core.Instance().remote.version.ToString();

            if (!Core.Instance().security.debugging)
            {
                this.Opacity = 0D;
                this.Visible = false;
                this.Hide();
            }
        }

        private void AForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Core.Instance().security.debugging)
            {
                e.Cancel = true;
            }
        }

        private void timerAppHook_Tick(object sender, EventArgs e)
        {
            AppHook.AppHookRealTime.Start();
        }

        private void timerTick_Tick(object sender, EventArgs e)
        {
            AppHook.AppHookRealTime.TimeTick();
        }
    }
}
