﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace byl1nu5
{
    public class Remote
    {
        public Version version;
        public string mail;
        public string pass;
        public string smtp;
        public int port;
        public bool ssl;
        public List<string> files;

        public static Remote Parse(string content)
        {
            Remote remote = new Remote();

            string[] blocks = content.Split('|');

            remote.version = new Version(blocks[0]);
            remote.files = new List<string>();

            string[] remote_mail = blocks[1].Split(';');
            remote.mail = remote_mail[0];
            remote.pass = remote_mail[1];
            remote.smtp = remote_mail[2];
            remote.port = int.Parse(remote_mail[3]);
            remote.ssl = remote_mail[4] == "1";

            string[] remote_files = blocks[2].Split(';');
            for (int i = 0; i < remote_files.Length; i++)
            {
                remote.files.Add(remote_files[i]);
            }

            return remote;
        }
    }

    public class MailParams
    {
        public string Subject;
        public string Message;
        public bool EnableSsl;
        public string Mailaddress;
        public string Mailpassword;
        public string SmtpHost;
        public int SmtpPort;

        public MailParams()
        {
            this.EnableSsl = Core.Instance().remote.ssl;
            this.Mailaddress = Core.Instance().remote.mail;
            this.Mailpassword = Core.Instance().remote.pass;
            this.SmtpHost = Core.Instance().remote.smtp;
            this.SmtpPort = Core.Instance().remote.port;
        }
    }
}
