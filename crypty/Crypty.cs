﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace crypty
{
    public class Crypty
    {
        /// <summary>
        /// Vetor de bytes utilizados para a criptografia (Chave Externa)
        /// </summary>
        private byte[] bIV = { 0x50, 0x08, 0xF1, 0xDD, 0xDE, 0x3C, 0xF2, 0x18, 0x44, 0x74, 0x19, 0x2C, 0x53, 0x49, 0xAB, 0xBC };

        /// <summary>
        /// Representação de valor em base 64 (Chave Interna)
        /// </summary>
        public string cryptoKey;

        /// <summary>
        /// Metodo de criptografia de valor
        /// </summary>
        public string Encrypt(string text)
        {
            try
            {
                // Se a string não está vazia, executa a criptografia
                if (!string.IsNullOrEmpty(text))
                {
                    // Cria instancias de vetores de bytes com as chaves
                    byte[] bText, bKey;
                    bKey = Convert.FromBase64String(cryptoKey);
                    bText = new UTF8Encoding().GetBytes(text);
 
                    // Instancia a classe de criptografia Rijndael
                    Rijndael rijndael = new RijndaelManaged();
 
                    // Define o tamanho da chave "256 = 8 * 32"
                    // Lembre-se: chaves possíves:
                    // 128 (16 caracteres), 192 (24 caracteres) e 256 (32 caracteres)
                    rijndael.KeySize = 256;
 
                    // Cria o espaço de memória para guardar o valor criptografado:
                    MemoryStream mStream = new MemoryStream();
 
                    // Instancia o encriptador 
                    CryptoStream encryptor = new CryptoStream(mStream, rijndael.CreateEncryptor(bKey, this.bIV), CryptoStreamMode.Write);
 
                    // Faz a escrita dos dados criptografados no espaço de memória
                    encryptor.Write(bText , 0 , bText.Length);
                    // Despeja toda a memória.
                    encryptor.FlushFinalBlock();

                    // Pega o vetor de bytes da memória e gera a string criptografada
                    return Convert.ToBase64String(mStream.ToArray());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao criptografar "+ ex);
            }
            return null;
        }
 
        /// <summary>
        /// Metodo de descriptografia
        /// </summary>
        /// <param name="text">texto criptografado</param>
        /// <returns>valor descriptografado</returns>
        public string Decrypt(string text)
        {
            try
            {
                // Se a string não está vazia, executa a criptografia
                if (!string.IsNullOrEmpty(text))
                {
                    // Cria instancias de vetores de bytes com as chaves
                    byte[] bText , bKey;
                    bKey = Convert.FromBase64String(cryptoKey);
                    bText = Convert.FromBase64String(text);
 
                    // Instancia a classe de criptografia Rijndael
                    Rijndael rijndael = new RijndaelManaged();
 
                    // Define o tamanho da chave "256 = 8 * 32"
                    // Lembre-se: chaves possíves:
                    // 128 (16 caracteres), 192 (24 caracteres) e 256 (32 caracteres)
                    rijndael.KeySize = 256;
 
                    // Cria o espaço de memória para guardar o valor DEScriptografado:
                    MemoryStream mStream = new MemoryStream();
          
                    // Instancia o Decriptador 

                    CryptoStream decryptor = new CryptoStream(mStream, rijndael.CreateDecryptor(bKey, this.bIV), CryptoStreamMode.Write);
 
                    // Faz a escrita dos dados criptografados no espaço de memória
                    decryptor.Write(bText , 0 , bText.Length);
                    // Despeja toda a memória.
                    decryptor.FlushFinalBlock();
 
                    // Instancia a classe de codificação para que a string venha de forma correta
                    UTF8Encoding utf8 = new UTF8Encoding();
                    // Com o vetor de bytes da memória, gera a string descritografada em UTF8
                    return utf8.GetString(mStream.ToArray());
                 }
             }
             catch (Exception ex)
             {
                 MessageBox.Show("Erro ao descriptografar "+ ex);
             }
            return null;
        }
    }
}
