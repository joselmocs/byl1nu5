﻿using System;
using System.Text;
using System.Windows.Forms;


namespace crypty
{
    public partial class Form1 : Form
    {
        protected Crypty crypt;
        protected bool criptografado;

        public Form1()
        {
            InitializeComponent();
            this.crypt = new Crypty();
        }

        public string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = ASCIIEncoding.UTF8.GetBytes(toEncode);
            return Convert.ToBase64String(toEncodeAsBytes);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!this.criptografado)
            {
                if (this.textBoxKey.Text.Length != 32)
                {
                    MessageBox.Show("Sua Chave deve conter 32 caracteres.");
                }
                else if (this.textBoxText.Text.Length < 1)
                {
                    MessageBox.Show("Você precisa de um texto para criptografar.");
                }
                else
                {
                    crypt.cryptoKey = this.EncodeTo64(this.textBoxKey.Text);
                    this.textBoxKeyCrypt.Text = crypt.cryptoKey;
                    this.textBoxText.Text = crypt.Encrypt(this.textBoxText.Text);

                    this.criptografado = true;
                    this.textBoxText.ReadOnly = true;
                    this.textBoxKey.ReadOnly = true;
                    this.button1.Text = "Descriptografar";
                }
            }
            else
            {
                this.textBoxText.Text = crypt.Decrypt(this.textBoxText.Text);
                this.criptografado = false;
                this.textBoxText.ReadOnly = false;
                this.textBoxKey.ReadOnly = false;
                this.button1.Text = "Criptografar";
            }
        }
    }
}
